from appointment import Appointment
from datetime import datetime
no_color = False
try:
    from termcolor import colored
except ModuleNotFoundError:
    no_color = True


def print_colored(text, color="red"):
    '''
    Prints the provided string in provided color if user has termcolor module

    Parameters:
        text (str): The string to be printed out
        color (str) (defaults to "red"): The color the text should be printed
    '''
    if no_color:
        print(text)
    else:
        print(colored(text, color))


def print_cow():
    '''
    Prints a blue cow saying PyLendar

    Credit to https://github.com/tnalpgge/rank-amateur-cowsay
    for the ascii art
    '''
    print_colored(
        "\
 __________\n\
< PyLendar >\n\
 ---------- \n\
        \\   ^__^\n\
         \\  (oo)\\_______\n\
            (__)\\       )\\/\\\n\
                ||----w |\n\
                ||     ||", "blue")


def format_appointment_input(*, old=None):
    '''
    Gets an appointment object from the user input

    Asks the user to provide name, address, year
        month, day, hour and minute of the appointment
    If old is provided and user enters nothing for one of
        the above-mentioned fields, the data from
        the old appointment will be used instead
    If the user provides unparsable values for date/time
        fields, they get asked to try again,
        should they decline the function returns False

    Parameters:
        Keyword-only old (appointment) (optional, defaults to None):
            An apointment be used for default values

    Returns:
        appointment: with the attributes set to user input
        bool: False, if the operation was aborted
    '''
    try:
        print("Use Ctrl+C to abort the operation")
        name = input("Name of the appointment: ")
        address = input("Appointment address: ")
        print("Enter Appointment date and time")
        print("Please numbers for the following fields")
        year = input("Year: ")
        month = input("Month: ")
        day = input("Day: ")
        hour = input("Hour: ")
        minute = input("Minute: ")
    except KeyboardInterrupt:
        print_colored("\nAborted")
        print("Use CTRL+C again if you want to quit")
        return False
    if (old is not None):
        if name == "":
            name = old.name
        if address == "":
            address = old.address
        if year == "":
            year = old.date_obj.year
        if month == "":
            month = old.date_obj.month
        if day == "":
            day = old.date_obj.day
        if hour == "":
            hour = old.date_obj.hour
        if minute == "":
            minute = old.date_obj.minute
    while True:
        try:
            date1 = f"{day}/{month}/{year}, {hour}:{minute}"
            date_obj = datetime.strptime(date1, '%d/%m/%Y, %H:%M')
        except ValueError:
            print_colored("Bad input. Try again?")
            if input("yes/no: ").lower() in "yes1":
                print_colored("Use integers for each field", "yellow")
                year = input("Year: ")
                month = input("Month: ")
                day = input("Day: ")
                hour = input("Hour: ")
                minute = input("Minute: ")
            else:
                return False
        else:
            break
    return Appointment(name, address, date_obj)


def print_appointment(appointment, color="green"):
    '''
    Prints an appointment in a colored, human-readable format

    Parameters:
        appointment (Appointment): The appointment to be printed out
        color (str) (defaults to green): The color of the resulting text
    '''
    print_colored(appointment.to_fancy_string(), color)


def print_all_appointments(appointments, *, limit=None, search=""):
    '''
    Prints all appointments in a nice, human-readable format

    Parameters:
        appointments (Appointment[]): The array of appointments
        limit (int) (defaults to None): Max amount of appointments printed
        search (str) (defaults to None): Prints only apointments containing
            the search string in name or address
    '''
    if limit is None:
        limit = len(appointments)
    color = "green"
    count = 0
    for a in appointments:
        if search.lower() in a.name.lower() + a.address.lower():
            print_appointment(a, color)
            if color == "blue":
                color = "green"
            else:
                color = "blue"
            count += 1
        if (count >= limit):
            break


def read_from_file(file_r):
    '''
    Reads the data from a file

    Reads data from the provided file_location
    This function expects the file to be formated in a way
        provided by appointment.to_string() with each
        appointment occupiying one line
    If the string is in a wrong format,
        a notification will be printed out
        and the line will be disregarded

    Parameters:
        file (file): The file that should be read
    Returns:
        list: The list of appointments base on file's data
    Throws:
        FileNotFoundError: if the file does not exist
    '''
    appointments_strings = file_r.readlines()
    appointments = []
    for appointment_string in appointments_strings:
        if appointment_string != "\n":
            try:
                appointment_array = appointment_string.split(":|:")
                aa = appointment_array
                appointments.append(Appointment(aa[1],
                                    aa[2],
                                    datetime.fromtimestamp(float(aa[3]))))
            except IndexError:
                print_colored("The file includes a corrupted line: "
                              + appointment_string)
    return appointments


def write_to_file(appointments, file_w, *, fancy=False):
    '''
    Stores the provided array of appointments to the  provided file

    Parameters:
        appointment_array (appointment[]): the list of appointments
        file (file): the file the list should be stored it
        Keyword-only fancy (bool) (optional, defaults to False:
            specified if the appointments should be written in a fancy format
    '''
    file_w.seek(0)
    file_w.truncate()
    appointments_strings = []
    if fancy:
        for appointment in appointments:
            appointments_strings.append(f"{appointment.to_fancy_string()}\n")
    else:
        for appointment in appointments:
            appointments_strings.append(appointment.to_string())
    file_w.writelines(appointments_strings)


def create_and_test_file(file_name):
    '''
    Creates the file and checks for errors

    Creates the file with the provided name / path
        (current directory will be used only name is provided)
        if it did not exist before
    File contents remain untouched
    If errors were encountered, a red text will be printed out
        with a brief description and a tip on how to mitigate the error

    Parameters:
        file_name (str): The path and name of the file

    Returns:
        bool: False, if there errors were incountered
        bool: True, if everything went smoothly
    '''
    try:
        with open(file_name, "a"):
            pass
    except PermissionError:
        print_colored(
            f"Could not open the file \"{file_name}\": Permission Denied")
        print_colored("Maybe try sudo?")
        return False
    except IsADirectoryError:
        print_colored(
            f"Could not open the file \"{file_name}\": It is a directory")
        print_colored("Check the provided file path")
        return False
    else:
        return True


def unmet_dependancies_message():
    if no_color:
        print("Module termcolor can't be found")
        print("try 'pip install -r requirements.txt'")
        print("For now the output will not be colored")
