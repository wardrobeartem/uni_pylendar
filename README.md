# uni_finalProject

### The task

Program an appointment calendar that allows you to enter
appointments. An entry consists of an event name, a location, a
date (year, month, day, time)\
Save the entries in a suitable structure array with up to 100 entries.\
Make it possible to enter new appointments with your dates and a
list function that displays the entire appointment calendar.
Write and read your data to and from a file called "calendar.txt"

### How can I contribute?

Please don't
This is just my group's project

### How can I run the program?

#### Dependencies
You need an up-to-date version of python3\
We have tested with python version >= 3.9\
You could install optional dependancies for quality-of-life features


#### Instalation and Running
```
git clone https://github.com/WardrobeArtem/uni_finalProject.git
cd uni_finalProject
pip install -r requirements.txt # optional dependencies
python pylendar.py
```

#### New features
New code is added to feature branches first\
You can use the following commands to ckeckout new features
```
git branch -a             # lists all branches
git checkout branchName   # use branchName of the branch you want to checkout
```
