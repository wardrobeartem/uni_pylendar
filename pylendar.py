import core


def main_menu():
    '''
    Prints a menu and asks for input

    Prints all available choices as a list
    Asks the user which one they pick
    Calles a function depending on users choice
    This repeats until user selects an option that facilitates quiting
    '''
    while True:
        print("-------------")
        print("1. Add an appointment ...")
        print("2. Manage individual appointments ...")
        print("3. List all appointments ...")
        print("4. Delete all appointments")
        print("5. About")
        print("6. Data ...")
        print("9. Save and Quit")
        print("0. Quit without Saving")
        option = input("Choose an option: ")
        print("\n" * 100)
        if option == "1" or option == "+":
            option_add_appointment()
        elif option == "2":
            menu_manage_appointments()
        elif option == "3":
            menu_list_appointments()
        elif option == "4":
            option_delete_appointments()
        elif option == "5":
            option_about()
        elif option == "6":
            menu_data()
        elif option == "9":
            option_save_and_quit()
            return
        elif option == "0":
            if option_quit_wo_saving():
                return


def menu_list_appointments():
    '''
    Prints all appointment and provides sorting / filtering options
    '''
    if not len(appointments):
        core.print_colored("There are no appointments to list")
        return
    reverse = False
    max_entries = len(appointments)
    option = "0"
    while (option != "9"):
        core.print_all_appointments(appointments, limit=max_entries)
        if (max_entries == 1):
            print(f"1. Display {max_entries} entry")
        else:
            print(f"1. Display {max_entries} entries")
        print("2. Sort by name")
        print("3. Sort by address")
        print("4. Sort by date")
        if reverse:
            print("5. Zz-Aa")
        else:
            print("5. aA-zZ")
        print("9. Exit sub-menu")
        option = input("Choose an option: ")
        if (option == "1"):
            max_entries = int(input(
                "What should be the maximum amount of entries displayed?: "))
        if (option == "2"):
            appointments.sort(key=lambda x: x.name, reverse=reverse)
        if (option == "3"):
            appointments.sort(key=lambda x: x.address, reverse=reverse)
        if (option == "4"):
            appointments.sort(key=lambda x: x.date_obj.timestamp(),
                              reverse=reverse)
            pass
        if (option == "5"):
            appointments.reverse()
            reverse = not reverse
        print("\n" * 100)
    return


def menu_manage_appointments():
    '''
    Prints the first appointment and provides options

    Prints the first appointment in a nice way
    The user can then cycle through appointments and perform actions on them
    '''
    if not len(appointments):
        core.print_colored("There are no appointments to manage")
        return
    current_index = 0
    array_max = len(appointments) - 1
    option = "0"
    while (option != "9"):
        if current_index % 2 == 0:
            color = "green"
        else:
            color = "blue"
        print(f"Appointment {current_index + 1} out of {array_max + 1}")
        core.print_appointment(appointments[current_index], color)
        print("-------------")
        print("1. Previous appointment <-")
        print("2. Modify selected appointment")
        print("3. Delete selected appointment")
        print("4. Next appointment ->")
        print("9. Exit sub-menu")
        option = input("Choose an option: ")
        print("\n" * 100)
        if option == "1":
            if current_index > 0:
                current_index -= 1
            else:
                current_index = array_max
        elif option == "2":
            option_modify_appointment(appointments, current_index)
        elif option == "3":
            appointments.pop(current_index)
            core.print_colored("Appointment deleted", "green")
            current_index = max(current_index - 1, 0)
            array_max -= 1
            if array_max == -1:
                return
        elif option == "4":
            if current_index < array_max:
                current_index += 1
            else:
                current_index = 0
    return


def menu_data():
    '''
    Prints a menu with options related to data storage
    '''
    option = "0"
    global appointments
    backup_file_name = "pylendar.backup"
    while option != 9:
        print("What to do with data?")
        print("1. Create a backup")
        print("2. Load a backup")
        print("3. Export appointments as a file")
        print("4. Save changes")
        print("9. Exit sub-menu")
        option = input("Choose an option: ")
        print("\n" * 100)
        if option == "1":
            if core.create_and_test_file(backup_file_name):
                with open(backup_file_name, "w") as f:
                    core.convert_array_to_file(appointments, f)
                    core.print_colored(f"Backup file \"{backup_file_name}\""
                                       + " created", "green")
            else:
                print("Failed to create backup")
        elif option == "2":
            print("Are you sure you want to load appointments from a backup?")
            print("This will overwrite your existing appointments")
            if input("yes/no: ").lower() in "yes1":
                try:
                    with open(backup_file_name, "r") as f:
                        appointments = core.read_from_file(f)
                        core.print_colored(f"Loaded {len(appointments)}"
                                           + " appointments from backup",
                                           "green")
                except FileNotFoundError:
                    core.print_colored("Backup file not found")
                    core.print_colored(f"Make sure \"{backup_file_name}\""
                                       + " exists in the current directory")
        elif option == "3":
            option_export_to_file()
        elif option == "4":
            core.write_to_file(appointments, calendar_file)
            print(f"Changes sucessfully saved to {file_location}\n")
        elif option == "9":
            return


# option_add_appointment
# asks for user input and appends the given appointment to appointments[]
def option_add_appointment():
    print("\n-------------")
    print("Adding a new appointment")
    new_appointment = core.format_appointment_input()
    if new_appointment:
        appointments.append(new_appointment)
        core.print_colored(f"Appointment {new_appointment.name} added\n",
                           "green")
    else:
        core.print_colored("Appointment has not been added\n")


def option_delete_appointments():
    '''
    Clears appointments list
    '''
    appointments.clear()
    core.print_colored("All appointments deleted", "green")


def option_about():
    '''
    Prints out info about the program
    '''
    core.print_cow()
    print("\nMade by Oliver & Georgii\n")
    print("A FOSS CLI Calendar app written in python")
    print("Manages appointments fast")
    return


def option_save_and_quit():
    '''
    Writes data to calendar_file and prints bye bye
    '''
    print("bye bye")
    core.write_to_file(appointments, calendar_file)
    return


def option_quit_wo_saving():
    '''
    Asks for user coonfirmation before quiting

    Returns:
        bool: True if the user confirms quiting
        bool: False if the user aborts quiting or provides bad input
    '''
    print("Are you sure you want to quit?")
    print("All changes you made in this session will be discarded")
    if input("yes/no: ").lower() in "yes1":
        print("bye bye")
        return True
    return False


def option_export_to_file():
    '''
    Writes the appointments to a provided file
    '''
    print("The file will be created in the current directory"
          + "unless a full path is provided")
    print("If a file with the same name you provide already exists"
          + "it will be overwritten")
    print("Choose a name for the export file or type ZQ to cancel")
    file_name = input("> ")
    restricted_characters = "<>:\"/\\|?*"
    if file_name == "ZQ":
        print("Aborting\n")
        return
    elif any(c in restricted_characters for c in file_name):
        print("Depending on you the OS you are running, "
              + f"the provided name: \"{file_name}\" "
              + "might cause some problems")
        print("Are you sure you want to proceed with this name?")
        if input("yes/no: ").lower() not in "yes1":
            core.print_colored("Aborting\n")
            return
    if not core.create_and_test_file(file_name):
        core.print_colored("Could not export appointments due to an error\n")
        return
    with open(file_name, "w") as f:
        core.write_to_file(appointments, f, fancy=True)
        core.print_colored(f"Appointments exported to {file_name}\n", "green")


def option_modify_appointment(appointments, index):
    print("Please provide updated data for the appointment")
    print("Leaving a field blank keeps the original data")
    new_appointment =\
        core.format_appointment_input(old=appointments[index])
    if new_appointment:
        appointments[index] = new_appointment
        core.print_colored("Appointment modified\n", "green")
    else:
        core.print_colored("Appointment has not been modified\n")


def main():
    global file_location
    global calendar_file
    global appointments
    print("\n" * 100)
    file_location = "calendar.txt"
    core.print_cow()
    print("\nMade by Oliver & Georgii\n")
    core.unmet_dependancies_message()
    if not core.create_and_test_file(file_location):
        quit()
    with open(file_location, "r+") as calendar_file:
        appointments = core.read_from_file(calendar_file)
        main_menu()


if __name__ == "__main__":
    main()
