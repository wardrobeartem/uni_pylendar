class Appointment:
    '''
    This class describes an appointment

    Attributes:
        name (str): Appointment name
        address (str): Appointment address
        date_obj (datetime): Appointment date and time
    '''
    def __init__(self, name, address, date_obj):
        '''
        The constructor for appointment class.

        Parameters:
            name (str): Appointment name
            address (str): Appointment address
            date_obj (datetime): Appointment date and time
        '''
        self.name = name
        self.address = address
        self.date_obj = date_obj

    def to_string(self):
        '''
        Returns a formated string

        Returns:
            str: Concatenated atributes of appointment
                :|:name:|:address:|:unix.timestamp:|:\n
        '''
        return(":|:" + self.name
               + ":|:" + self.address
               + ":|:" + str(self.date_obj.timestamp()) + ":|:"
               + "\n")

    def to_fancy_string(self):
        '''
        Returns a formated string

        Returns:
            str: Fancy, human-readable representation
                Name: name
                Address: adreess
                DOW month dd hh:mm:ss yyyy
        '''
        return("Name: " + self.name + "\n"
               + "Address: " + self.address + "\n"
               + self.date_obj.ctime() + "\n")
